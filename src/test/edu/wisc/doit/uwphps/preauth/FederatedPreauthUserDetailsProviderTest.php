<?php
namespace edu\wisc\doit\uwphps\preauth;

use edu\wisc\doit\uwphps\EnvironmentHelper;

/**
 * Tests for {@link FederatedPreauthUserDetailsProvider}.
 */
class FederatedPreauthUserDetailsProviderTest extends \PHPUnit_Framework_TestCase
{

    use EnvironmentHelper;

    /** @var FederatedPreauthUserDetailsProvider */
    private $userProvider;

    /** @var FederatedPreauthUserDetailsProvider  configured to use HTTP headers */
    private $userProviderHttp;

    /** @var array  environment variables to be set when testing */
    private $environment;

    protected function setUp()
    {

        $this->userProvider = new FederatedPreauthUserDetailsProvider(false);
        $this->userProviderHttp = new FederatedPreauthUserDetailsProvider(true);

        // Default environment variables
        $this->environment = [
            FederatedPreauthUserDetailsProvider::FULL_NAME => 'BUCKINGHAM BADGER',
            FederatedPreauthUserDetailsProvider::EPPN => 'bbadger@wisc.edu',
            FederatedPreauthUserDetailsProvider::FIRST_NAME => 'BUCKINGHAM',
            FederatedPreauthUserDetailsProvider::EMAIL => 'bucky.badger@wisc.edu',
            FederatedPreauthUserDetailsProvider::SHIB_SESSION_ID => '1234567890',
            FederatedPreauthUserDetailsProvider::LAST_NAME => 'BADGER',
            FederatedPreauthUserDetailsProvider::SOURCE => 'a_source',
            FederatedPreauthUserDetailsProvider::SPVI => 'UW123A456',
            FederatedPreauthUserDetailsProvider::ISIS_EMPLID => '123456789',
            FederatedPreauthUserDetailsProvider::MEMBER_OF => 'A06;A07'
        ];
    }

    /** @test */
    public function loadsNullUserWhenNoSession()
    {
        $this->removeEnvironmentVariable(FederatedPreauthUserDetailsProvider::SHIB_SESSION_ID);
        $this->removeEnvironmentVariable(FederatedPreauthUserDetailsProvider::SHIB_SESSION_ID_HTTP);
        static::assertNull($this->userProvider->loadUser());
        static::assertNull($this->userProviderHttp->loadUser());
    }

    /** @test */
    public function loadsUser()
    {
        $this->setEnvironment($this->environment);
        $user = $this->userProvider->loadUser();

        static::assertNotNull($user);
        static::assertEquals($this->environment[FederatedPreauthUserDetailsProvider::EPPN], $user->getEppn());
        static::assertEquals($this->environment[FederatedPreauthUserDetailsProvider::SPVI], $user->getPvi());
        static::assertEquals($this->environment[FederatedPreauthUserDetailsProvider::FULL_NAME], $user->getFullName());
        static::assertEquals($this->environment[FederatedPreauthUserDetailsProvider::EMAIL], $user->getEmailAddress());
        static::assertEquals($this->environment[FederatedPreauthUserDetailsProvider::SOURCE], $user->getSource());
        static::assertEquals(
            $this->environment[FederatedPreauthUserDetailsProvider::ISIS_EMPLID],
            $user->getIsisEmplid()
        );
        static::assertEquals(
            $this->environment[FederatedPreauthUserDetailsProvider::FIRST_NAME],
            $user->getFirstName()
        );
        static::assertEquals($this->environment[FederatedPreauthUserDetailsProvider::LAST_NAME], $user->getLastName());
        static::assertEquals(
            explode(FederatedPreauthUserDetailsProvider::DELIMITER, $this->environment[FederatedPreauthUserDetailsProvider::MEMBER_OF]),
            $user->getIsMemberOf()
        );
    }

    /** @test */
    public function loadsUserWithHttpHeaders()
    {
        $this->setEnvironment($this->toHttpHeaders($this->environment));
        $user = $this->userProviderHttp->loadUser();

        static::assertNotNull($user);
        static::assertEquals($this->environment[FederatedPreauthUserDetailsProvider::EPPN], $user->getEppn());
        static::assertEquals($this->environment[FederatedPreauthUserDetailsProvider::SPVI], $user->getPvi());
        static::assertEquals($this->environment[FederatedPreauthUserDetailsProvider::FULL_NAME], $user->getFullName());
        static::assertEquals($this->environment[FederatedPreauthUserDetailsProvider::EMAIL], $user->getEmailAddress());
        static::assertEquals($this->environment[FederatedPreauthUserDetailsProvider::SOURCE], $user->getSource());
        static::assertEquals(
            $this->environment[FederatedPreauthUserDetailsProvider::ISIS_EMPLID],
            $user->getIsisEmplid()
        );
        static::assertEquals(
            $this->environment[FederatedPreauthUserDetailsProvider::FIRST_NAME],
            $user->getFirstName()
        );
        static::assertEquals($this->environment[FederatedPreauthUserDetailsProvider::LAST_NAME], $user->getLastName());
        static::assertEquals(
            explode(FederatedPreauthUserDetailsProvider::DELIMITER, $this->environment[FederatedPreauthUserDetailsProvider::MEMBER_OF]),
            $user->getIsMemberOf()
        );
    }

    /** @test */
    public function missingAttributeIsFalse()
    {
        $this->setEnvironment($this->environment);
        $this->removeEnvironmentVariable(FederatedPreauthUserDetailsProvider::EMAIL);
        $user = $this->userProvider->loadUser();
        static::assertNotNull($user);
        static::assertFalse($user->getEmailAddress());
    }
}
