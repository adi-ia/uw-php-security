<?php
namespace edu\wisc\doit\uwphps\local;

/**
 * Tests for {@link LocalUserDetailsProvider}.
 */
class LocalUserDetailsProviderTest extends \PHPUnit_Framework_TestCase
{

    private $resourcesDir = __DIR__ . '/../../../../../resources';

    /** @test */
    public function loadsUser()
    {
        $userDetailsService = new LocalUserDetailsProvider("{$this->resourcesDir}/localuser.json");
        $user = $userDetailsService->loadUser();
        $this->assertEquals("bbadger@wisc.edu", $user->getEppn());
        $this->assertEquals("UW123A456", $user->getPvi());
        $this->assertEquals("BUCKINGHAM BADGER", $user->getFullName());
        $this->assertEquals("bucky.badger@wisc.edu", $user->getEmailAddress());
        $this->assertEquals("a_source", $user->getSource());
        $this->assertEquals("123456789", $user->getIsisEmplid());
        $this->assertEquals("BUCKINGHAM", $user->getFirstName());
        $this->assertEquals("BADGER", $user->getLastName());
        $this->assertEquals(['A06', 'A07'], $user->getIsMemberOf());
    }

    /** @test */
    public function loadsNullUserWhenMissingFile()
    {
        $provider = new LocalUserDetailsProvider("{$this->resourcesDir}/nobody.json");
        $this->assertNull($provider->loadUser());
    }

    /**
     * @test
     * @expectedException \edu\wisc\doit\uwphps\local\JsonDecodingException
     */
    public function throwsIfInvalidJson()
    {
        $provider = new LocalUserDetailsProvider("{$this->resourcesDir}/badjson.json");
        $provider->loadUser();
    }

}
