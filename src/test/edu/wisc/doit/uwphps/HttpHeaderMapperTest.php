<?php
namespace edu\wisc\doit\uwphps;

use PHPUnit\Framework\TestCase;

/**
 * Tests for {@link HttpHeaderMapper} trait
 */
class HttpHeaderMapperTest extends TestCase
{
    use HttpHeaderMapper;

    /** @test */
    public function convertsAttributeToHttpHeader()
    {
        $this->assertEquals('HTTP_STARFLEETRANK', $this->httpHeaderFromAttribute('starfleetRank'));
        $this->assertEquals('HTTP_SHIB_SESSION_ID', $this->httpHeaderFromAttribute('Shib-Session-Id'));
    }
}
