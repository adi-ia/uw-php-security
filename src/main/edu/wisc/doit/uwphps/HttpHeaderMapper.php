<?php
namespace edu\wisc\doit\uwphps;

/**
 * Maps Shibboleth attributes to their HTTP header equivalents
 */
trait HttpHeaderMapper
{
    /**
     * Map a Shibboleth attribute to its associated HTTP header name.
     *
     * @param string $attribute attribute to map
     * @return string Shibboleth attribute name mapped to its equivalent HTTP header name
     *
     * @see https://wiki.shibboleth.net/confluence/display/SHIB2/NativeSPAttributeAccess NativeSPAttributeAccess
     */
    protected function httpHeaderFromAttribute($attribute)
    {
        return 'HTTP_' . strtoupper(str_replace('-', '_', $attribute));
    }
}
