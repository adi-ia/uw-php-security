<?php

namespace edu\wisc\doit\uwphps\preauth;

use edu\wisc\doit\uwphps\UserDetailsProvider;
use edu\wisc\doit\uwphps\UWUserDetails;

/**
 * FederatedPreauthUserDetailsProvider is an implementation of {@link UserDetailsProvider} for loading users
 * authenticated with UW-System Federated login.
 *
 * {@see PreauthUserDetailsProvider} for loading users authenticated through UW-Madison login.
 */
class FederatedPreauthUserDetailsProvider extends UserDetailsProvider
{

    // Constants defining common header values
    const SPVI = "eduWisconsinSPVI";
    const EMAIL = "eduWisconsinEmailAddress";
    const FULL_NAME = "eduWisconsinCommonName";
    const FIRST_NAME = "eduWisconsinGivenName";
    const LAST_NAME = "eduWisconsinSurname";

    /**
     * {@inheritdoc}
     */
    public function loadUser()
    {
        // Return null if no Shib session is found
        if (($this->httpHeaders && !getenv(static::SHIB_SESSION_ID_HTTP)) ||
            (!$this->httpHeaders && !getenv(static::SHIB_SESSION_ID))) {
            return null;
        }

        if ($this->httpHeaders) {
            $userDetails = new UWUserDetails(
                getenv($this->httpHeaderFromAttribute(static::EPPN)),
                getenv($this->httpHeaderFromAttribute(static::SPVI)),
                getenv($this->httpHeaderFromAttribute(static::FULL_NAME)),
                explode(static::DELIMITER, getenv($this->httpHeaderFromAttribute(static::UDDS))),
                getenv($this->httpHeaderFromAttribute(static::EMAIL)),
                getenv($this->httpHeaderFromAttribute(static::SOURCE)),
                getenv($this->httpHeaderFromAttribute(static::ISIS_EMPLID)),
                getenv($this->httpHeaderFromAttribute(static::FIRST_NAME)),
                getenv($this->httpHeaderFromAttribute(static::LAST_NAME)),
                explode(static::DELIMITER, getenv($this->httpHeaderFromAttribute(static::MEMBER_OF)))
            );
        } else {
            $userDetails = new UWUserDetails(
                getenv(static::EPPN),
                getenv(static::SPVI),
                getenv(static::FULL_NAME),
                explode(static::DELIMITER, getenv(static::UDDS)),
                getenv(static::EMAIL),
                getenv(static::SOURCE),
                getenv(static::ISIS_EMPLID),
                getenv(static::FIRST_NAME),
                getenv(static::LAST_NAME),
                explode(static::DELIMITER, getenv(static::MEMBER_OF))
            );
        }

        return $userDetails;
    }

}
