<?php

namespace edu\wisc\doit\uwphps\preauth;

use edu\wisc\doit\uwphps\UserDetailsProvider;
use edu\wisc\doit\uwphps\UWUserDetails;

/**
 * PreauthUserDetailsProvider is an implementation of {@link UserDetailsProvider} for loading users authenticated
 * with UW-Madison login.
 *
 * See {@link FederatedPreauthUserDetailsProvider} for loading users authenticated through UW-System Federated login.
 */
class PreauthUserDetailsProvider extends UserDetailsProvider
{

    // Constants defining common header values
    const PVI = 'wiscEduPVI';
    const FULL_NAME = 'cn';
    const EMAIL = 'mail';
    const UDDS = 'wiscEduUDDS';
    const ISIS_EMPLID = 'wiscEduIsisEmplid';
    const FIRST_NAME = 'givenName';
    const LAST_NAME = 'sn';

    public function loadUser()
    {
        // Return null if no Shib session is found
        if (($this->httpHeaders && !getenv(static::SHIB_SESSION_ID_HTTP)) ||
            (!$this->httpHeaders && !getenv(static::SHIB_SESSION_ID))) {
            return null;
        }

        if ($this->httpHeaders) {
            $userDetails = new UWUserDetails(
                getenv($this->httpHeaderFromAttribute(static::EPPN)),
                getenv($this->httpHeaderFromAttribute(static::PVI)),
                getenv($this->httpHeaderFromAttribute(static::FULL_NAME)),
                explode(static::DELIMITER, getenv($this->httpHeaderFromAttribute(static::UDDS))),
                getenv($this->httpHeaderFromAttribute(static::EMAIL)),
                getenv($this->httpHeaderFromAttribute(static::SOURCE)),
                getenv($this->httpHeaderFromAttribute(static::ISIS_EMPLID)),
                getenv($this->httpHeaderFromAttribute(static::FIRST_NAME)),
                getenv($this->httpHeaderFromAttribute(static::LAST_NAME)),
                explode(static::DELIMITER, getenv($this->httpHeaderFromAttribute(static::MEMBER_OF)))
            );
        } else {
            $userDetails = new UWUserDetails(
                getenv(static::EPPN),
                getenv(static::PVI),
                getenv(static::FULL_NAME),
                explode(static::DELIMITER, getenv(static::UDDS)),
                getenv(static::EMAIL),
                getenv(static::SOURCE),
                getenv(static::ISIS_EMPLID),
                getenv(static::FIRST_NAME),
                getenv(static::LAST_NAME),
                explode(static::DELIMITER, getenv(static::MEMBER_OF))
            );
        }

        return $userDetails;
    }
}
