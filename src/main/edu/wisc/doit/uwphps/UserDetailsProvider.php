<?php

namespace edu\wisc\doit\uwphps;

/**
 * UseDetailsProvider is an interface defining how to load an {@link UserDetails} and provides constants for
 * supplied request header keys.
 */
abstract class UserDetailsProvider
{
    use HttpHeaderMapper;

    /** @var bool Flag indicating if headers are passed prefixed with 'HTTP_' */
    protected $httpHeaders;
    
    // General, shared constants relevant to UWMSN and UW-System login
    const EPPN = "eppn";
    const SHIB_SESSION_ID = 'Shib-Session-Id';
    const SHIB_SESSION_ID_HTTP = 'HTTP_SHIB_SESSION_ID';
    const UDDS = "udds";
    const SOURCE = "source";
    const ISIS_EMPLID = "isisEmplid";
    const MEMBER_OF = "isMemberOf";

    /** Delimiter used by multi-valued headers */
    const DELIMITER = ';';

    /**
     * UserDetailsProvider constructor.
     *
     * @param bool $http  true if Shibboleth attributes are delivered via HTTP request headers
     */
    public function __construct($http = false)
    {
        $this->httpHeaders = $http;
    }

    /**
     * Returns the logged-in user
     *
     * @return UserDetails  currently authenticated user, null if not logged in
     */
    abstract public function loadUser();
}
