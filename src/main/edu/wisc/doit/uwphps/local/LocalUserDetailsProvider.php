<?php

namespace edu\wisc\doit\uwphps\local;

use edu\wisc\doit\uwphps\preauth\PreauthUserDetailsProvider;
use edu\wisc\doit\uwphps\UWUserDetails;

/**
 * LocalUserDetailsProvider provides a developer with a {@link UWUserDetails} suitable for use in local development.
 */
class LocalUserDetailsProvider extends PreauthUserDetailsProvider
{

    /** @var string */
    private $filePath;

    /**
     * LocalUserDetailsProvider constructor.
     *
     * @param $filePath string Path to JSON file defining a local user.
     */
    public function __construct($filePath)
    {
        parent::__construct(false);
        $this->filePath = $filePath;
    }

    /**
     * Loads user from configured JSON file.
     *
     * @return UWUserDetails  user, null if file is missing
     * @throws JsonDecodingException  if unable to decode user JSON file
     */
    public function loadUser()
    {
        if (!is_readable($this->filePath)) {
            return null;
        }

        $attributes = json_decode(file_get_contents($this->filePath), true);

        // Throw exception if file cannot be decoded
        if ($attributes === null) {
            throw new JsonDecodingException('Unable to parse JSON in file: ' . realpath($this->filePath));
        }

        return new UWUserDetails(
            $attributes[static::EPPN],
            $attributes[static::PVI],
            $attributes[static::FULL_NAME],
            explode(static::DELIMITER, $attributes[static::UDDS]),
            $attributes[static::EMAIL],
            $attributes[static::SOURCE],
            $attributes[static::ISIS_EMPLID],
            $attributes[static::FIRST_NAME],
            $attributes[static::LAST_NAME],
            explode(static::DELIMITER, $attributes[static::MEMBER_OF])
        );
    }

    /**
     * Get the path of the local user JSON file.
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set the path of the local user JSON file.
     * @param $filePath
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }

}